import { createRouter, createWebHistory } from "@ionic/vue-router";
import { RouteRecordRaw } from "vue-router";


const routes: Array<RouteRecordRaw> = [
  {
    path: "/",
    redirect: "/tabs/dressings",
  },
  {
    path: "/home/",
    component: () => import("@/views/HomePage.vue"),
  },
  {
    path: "/login",
    component: () => import("@/views/LoginPage.vue"),
  },
  {
    path: "/signup",
    component: () => import("@/views/SignUpPage.vue"),
  },
  {
    path: "/create_dressing",
    component: () => import("@/views/AddDressing.vue"),
  },
  {
    path: "/create_vetement",
    component: () => import("@/views/AddVetement.vue"),
  },
  {
    name: "add_clothe",
    path: "/vetement/ajouter",
    component: () => import("@/views/UpsertClothe.vue"),
  },
  {
    name: "update_clothe",
    path: "/vetement/:id",
    component: () => import("@/views/UpsertClothe.vue"),
  },
  {
    name: "add_dressing",
    path: "/dressing/ajouter",
    component: () => import("@/views/UpsertDressing.vue"),
  },
  {
    name: "update_dressing",
    path: "/dressing/:id",
    component: () => import("@/views/UpsertDressing.vue"),
  },
  {
    name: "show_dressing",
    path: "/dressing/show/:id",
    component: () => import("@/views/ShowDressing.vue"),
  },
   {
    name: "test",
    path: "/test",
    component: () => import("@/views/PageTest.vue"),
  },
  {
    name: "test2",
    path: "/test2",
    component: () => import("@/views/PageTest2.vue"),
  },
  {
    path: "/tabs/",
    component: () => import("@/views/TabsPage.vue"),
    children: [
      {
        path: "",
        redirect: "/tabs/dressings",
      },
      {
        path: "dressings",
        component: () => import("@/views/DressingPage.vue"),
      },
      // {
      //   path: "tenues",
      //   component: () => import("@/views/TenuesPage.vue"),
      // },
      {
        path: "chat",
        component: () => import("@/views/ChatTab.vue"),
      },
      {
        path: "calendrier",
        component: () => import("@/views/CalendarTab.vue"),
      },
      {
        path: "tenues",
        component: () => import("@/views/OutfitTab.vue"),
      },
      {
        path: "cgu",
        component: () => import("@/views/CGUList.vue"),
      },
      {
        name: "conseils",
        path: "conseils",
        component: () => import("@/views/ConseilsPage.vue"),
      },
      {
        path: "profil",
        component: () => import("@/views/ProfilPage.vue"),
      },
    ],
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
