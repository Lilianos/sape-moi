//Call API for outfit
import axios from "axios";

//passer dans une variable d'environement !!!!!!!!!!!!
const api_strapi = "http://benoitlouveau.duckdns.org:1337";

export default {
  /**
   *Get all outfits
   * @returns
   */
  async get_outfits() {
    try {
      const results = await axios
        .get(api_strapi + "/outfits?_sort=name:ASC")
        .then((result) => {
          return result.data;
        });
      console.debug(results);
      return results;
    } catch (error) {
      console.debug(error);
      throw error;
    }
  },

  /**
   * Get outfit by id
   * @param id
   * @returns
   */
  async get_outfit_by_id(id: number) {
    try {
      const results = await axios.get(api_strapi + "/outfits?id=" + id);
      console.debug(results.data);
      return results.data;
    } catch (error) {
      console.error(error);
      throw error;
    }
  },

  /**
   * Create outfit
   * @param outfit
   */
  async create_outfit(outfit: any) {
    try {
      const result = await axios.post(api_strapi + "/outfits/", outfit);
      console.debug(result);
    } catch (error) {
      console.error(error);
    }
  },

  /**
   * Get numbers of outfits
   * @returns
   */
  async count_outfits() {
    try {
      const result = await axios.get(api_strapi + "/outfits/count");
      return result.data;
    } catch (error) {
      console.error(error);
      throw error;
    }
  },

  //update
  async delete_outfit(outfit: any) {
    try {
      const result = await axios.delete(api_strapi + "/outfits/" + outfit.id);
      console.debug(result);
    } catch (error) {
      console.error(error);
      throw error;
    }
  },
};
