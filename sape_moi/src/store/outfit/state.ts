//state.ts https://vuex.vuejs.org/guide/state.html
export interface OutfitStateInterface {
  //prop: boolean;
  all_outfits: [];
  user_outfits: [];
  one_outfit: [];
  current_outfit: [];
  selected_outfit: [];
  count_outfits: number;
  count_user_outfits: number;
}

function state(): OutfitStateInterface {
  return {
    //prop: false,
    all_outfits: [],
    user_outfits: [],
    one_outfit: [],
    current_outfit: [],
    selected_outfit: [],
    count_outfits: 0,
    count_user_outfits: 0,
  };
}

export default state;
