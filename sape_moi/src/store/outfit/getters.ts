//getters.ts https://vuex.vuejs.org/guide/getters.html
import { GetterTree } from "vuex";
import { StateInterface } from "../index";
import { OutfitStateInterface } from "./state";

//List of all getters
const getters: GetterTree<OutfitStateInterface, StateInterface> = {
  // someAction(/* context */) {
  //   // your code
  // },
  someAction(/* context */) {
    // your code
  },
};

export default getters;
