//actions.ts https://vuex.vuejs.org/guide/actions.html
import { ActionTree } from "vuex";
import { StateInterface } from "../index";
import { OutfitStateInterface } from "./state";

import repository from "./repository";

//List of all actions (dispatch)
const actions: ActionTree<OutfitStateInterface, StateInterface> = {
  // someAction(/* context */) {
  //   // your code
  // },
  someAction(/* context */) {
    // your code
  },
};

export default actions;
