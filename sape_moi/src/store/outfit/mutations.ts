//mutations.ts https://vuex.vuejs.org/guide/mutations.html
import { MutationTree } from "vuex";
import { OutfitStateInterface } from "./state";

//List of all mutations (commit)
const mutation: MutationTree<OutfitStateInterface> = {
  // someMutation(/* state: ExampleStateInterface */) {
  //   // your code
  // },
  someMutation(/* state: ExampleStateInterface */) {
    // your code
  },
};

export default mutation;
