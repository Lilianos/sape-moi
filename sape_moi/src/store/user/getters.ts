//getters.ts https://vuex.vuejs.org/guide/getters.html
import { GetterTree } from "vuex";
import { StateInterface } from "../index";
import state, { UserStateInterface } from "./state";

//List of all getters
const getters: GetterTree<UserStateInterface, StateInterface> = {
  // someAction(/* context */) {
  //   // your code
  // },
  current_user: state => state.current_user,
  access_token: state => state.access_token,
  tmp_user: state => state.tmp_user
};

export default getters;
