//actions.ts https://vuex.vuejs.org/guide/actions.html
import { ActionTree } from "vuex";
import { StateInterface } from "../index";
import { UserStateInterface } from "./state";
import axios from "axios";

import repository from "./repository";

//List of all actions (dispatch)
const actions: ActionTree<UserStateInterface, StateInterface> = {
  // someAction(/* context */) {
  //   // your code
  // },

  /**
   *
   * @param param0
   * @param {string} payload.email
   * @param {string} payload.password
   */
  async login({ commit }, payload) {
    const { email, password } = payload;
    try {
      const { user, jwt } = await repository.login(email, password);

      commit("set_current_user", user);
      commit("set_access_token", jwt);
      axios.defaults.headers.common["Authorization"] = `Bearer ${jwt}`;
    } 
    catch (error) {
      console.error(error);
      throw error;
    }
  },

  async logout({ commit }) {
    commit("set_current_user", null); // null ?
    commit("set_access_token", null); // null ? !!!!!!!!!!!!!
    delete axios.defaults.headers.common["Authorization"];
  },


  // async register({ state, commit }, ) {
  //   try {
  //     commit('UPDATE_TMP_USER', {
  //       username: state.tmpUser.email
  //     })
  //     const email = await repository.register(state.tmpUser, diagnostic)

  //     commit('SET_CURRENT_USER', null)
  //     commit('SET_ACCESS_TOKEN', null)
  //     delete axios.defaults.headers.common['Authorization']

  //     const { user, jwt } = await AuthRepository.login(
  //       state.tmpUser.email,
  //       state.tmpUser.password
  //     )

  //     commit('SET_CURRENT_USER', user)
  //     commit('SET_ACCESS_TOKEN', jwt)

  //     axios.defaults.headers.common['Authorization'] = `Bearer ${jwt}`

  //     return user
  //   } catch (err) {
  //     throw err.response.data
  //   }
  // },

  async FORGOT_PASSWORD({ commit }, payload) {
    try {
      const { email } = payload
      await repository.forgot_password(email)
      commit('set_reset_password_email', email)
    } catch (error) {
      console.error(error)
      throw error
    }
  },

};



export default actions;
