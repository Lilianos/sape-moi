//index.ts Module https://vuex.vuejs.org/guide/modules.html#modules
import { Module } from "vuex";
import { StateInterface } from "../index";
import state, { UserStateInterface } from "./state";
import actions from "./actions";
import getters from "./getters";
import mutations from "./mutations";

const clotheModule: Module<UserStateInterface, StateInterface> = {
  namespaced: true,
  actions,
  getters,
  mutations,
  state,
};

export default clotheModule;
