//state.ts https://vuex.vuejs.org/guide/state.html
export interface UserStateInterface {
    //prop: boolean;
    access_token: string;
    current_user: [];
    tmp_user: [];
  }
  
  function state(): UserStateInterface {
    return {
      //prop: false,
      access_token: '',
      current_user: [],
      tmp_user: [],
    };
  }
  
  export default state;