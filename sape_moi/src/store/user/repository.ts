//Call API for User

import axios from "axios";

//passer dans une variable d'environement !!!!!!!!!!!!
const api_strapi = "http://benoitlouveau.duckdns.org:1337";

export default{

    /**
     * Register user with username, email and password
     * remplacer les variable email et password par un objet user reflechir !!!!!!!!!!!!!!!!!!!
     * 
     * After having registered, if you have set Enable email confirmation to ON, the user will 
     * receive a confirmation link by email. The user has to click on it to validate his/her registration.
     * 
     * @param username 
     * @param email 
     * @param password 
     * @returns  
     */
    async register(username:string, email:string, password:string){
        try {
           const {user, jwt} = await axios.post(`${api_strapi}/auth/local/register`, {
                username: username,
                email: email,
                password: password
           }).then(response => response.data)
           return {user, jwt}
        } catch (error) {
            console.error(error)
            throw error
        }
    },

    /**
     * If needed, re-send the confirmation email
     * @param email 
     */
    async send_email_confirmation(email:string) {
        try {
            const response = await axios.post(`${api_strapi}/auth/send-email-confirmation`, {
              email
            })
            console.log(response)
        } catch (error) {
            console.error(error)
            throw error
        }
      },

    /**
     * Login user
     * remplacer les variable email et password par un objet user reflechir !!!!!!!!!!!!!!!!!!!
     * @param email 
     * @param password 
     * @returns 
     */
    async login(email: string, password : string) {
        try {
          const { user, jwt } = await axios
            .post(`${api_strapi}/auth/local`, {
              identifier: email,
              password,
            })
            .then(response => response.data)
          return { user, jwt }
        } catch (error: any) {
            console.error(error)
            throw error
        }
      },

      async forgot_password(email: string) {
        return await axios.post(`${api_strapi}/auth/forgot-password`, {
          email,
          url: `${process.env.APP.URL}/authentification/mot-de-passe/réinitialisation`
        })
      },

      // async reset_password(code, password, confirm) {
      //   return await axios.post(`${api_strapi}/auth/reset-password`, {
      //     code,
      //     password,
      //     passwordConfirmation: confirm
      //   })
      // },
    
      async modify_email(email: string, password: string) {
        return await axios
          .put(`${api_strapi}/users-permissions/me`, {
            email: email,
            password: password
          })
          .then(response => {
            return response.data
          })
      },

      async modify_password(newPassword: string, password: string) {
        return await axios
          .put(`${api_strapi}/users-permissions/me`, {
            newPassword: newPassword,
            password: password
          })
          .then(response => {
            return response.data
          })
      },
      
      // async modify_username(firstName, lastName, birthday, password) {
      //   return await axios
      //     .put(`${api_strapi}/users-permissions/me`, {
      //       firstName,
      //       lastName,
      //       birthday,
      //       password
      //     })
      //     .then(response => {
      //       return response.data
      //     })
      // },


}
