// VueX https://vuex.vuejs.org/
// store.ts https://vuex.vuejs.org/guide/typescript-support.html#typescript-support
import { InjectionKey } from "vue";
import {
  createStore,
  useStore as vuexUseStore,
  Store as VuexStore,
} from "vuex";

//import module => import example from './module-example'
// import { ExampleStateInterface } from './module-example/state';
import dressing from "./dressing";
import { DressingStateInterface } from "./dressing/state";
import clothe from "./clothe";
import { ClotheStateInterface } from "./clothe/state";
import outfit from "./outfit";
import { OutfitStateInterface } from "./outfit/state";

export interface StateInterface {
  // Define your own store structure, using submodules if needed
  // example: ExampleStateInterface;
  dressing: DressingStateInterface;
  clothe: ClotheStateInterface;
  outfit: OutfitStateInterface;
  // Declared as unknown to avoid linting issue. Best to strongly type as per the line above.
  //example: unknown;
}

// provide typings for `this.$store`
declare module "@vue/runtime-core" {
  interface ComponentCustomProperties {
    $store: VuexStore<StateInterface>;
  }
}

// define injection key
export const storeKey: InjectionKey<VuexStore<StateInterface>> =
  Symbol("vuex-key");

/**
 * createStore
 */
export const store = createStore<StateInterface>({
  modules: {
    // example
    dressing,
    clothe,
    outfit,
  },
  strict: true,
});

// define your own `useStore` composition function
export function useStore() {
  return vuexUseStore(storeKey);
}
