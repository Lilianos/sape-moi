//Call API for dressing
import axios from "axios";

//passer dans une variable d'environement !!!!!!!!!!!!
const api_strapi = "http://benoitlouveau.duckdns.org:1337";

export default {
  /**
   * Get all dressings
   * @returns
   */
  async get_dressings() {
    try {
      const results = await axios
        .get(api_strapi + "/dressings?_sort=name:ASC")
        .then((result) => {
          return result.data;
        });
      console.debug(results);
      return results;
    } catch (error) {
      console.error(error);
      throw error;
    }
  },

  /**
   * Get dressings by id
   * @param id
   * @returns
   */
  async get_dressing_by_id(id: number) {
    try {
      const results = await axios.get(api_strapi + "/dressings/" + id);
      return results.data;
    } catch (error) {
      console.error(error);
      throw error;
    }
  },

  /**
   * Create Dressing
   * @param dressing
   */
  async create_dressing(dressing: any) {
    try {
      const formData = new FormData();
      if (dressing.newImage) {
        console.log(dressing, dressing.newImage);
        formData.append("files.image", dressing.newImage);
      }
      formData.append("data", JSON.stringify(dressing));
      const result = await axios.post(api_strapi + "/dressings/", formData);
      console.log(result.data);
      return result.data;
    } catch (error) {
      console.error(error);
    }
  },

  /**
   * Get numbers of dressings
   * @returns
   */
  async count_dressings() {
    try {
      const result = await axios.get(api_strapi + "/dressings/count");
      return result.data;
    } catch (error) {
      console.error(error);
      throw error;
    }
  },

  async update_dressing(dressing: any) {
    try {
      const formData = new FormData();
      console.log(dressing);
      
      if (dressing.newImage) {
        console.log("debug");
        
        console.log(dressing, dressing.newImage);
        formData.append("files.image", dressing.newImage);
      }
      formData.append("data", JSON.stringify(dressing));
      const result = await axios.put(
        api_strapi + "/dressings/" + dressing.id,
        formData
      );
      console.debug(result);
      return result.data;
    } catch (error) {
      console.error(error);
      throw error;
    }
  },

  /**
   * Delete a dressing
   * @param dressing
   */
  async delete_dressing(dressing: any) {
    try {
      const result = await axios.delete(
        api_strapi + "/dressings/" + dressing.id
      );
      return result.data;
    } catch (error) {
      console.error(error);
      throw error;
    }
  },
};
