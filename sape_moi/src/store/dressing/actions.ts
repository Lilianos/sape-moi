//actions.ts https://vuex.vuejs.org/guide/actions.html
import { ActionTree } from "vuex";
import { StateInterface } from "../index";
import { DressingStateInterface } from "./state";

import repository from "./repository";

//List of all actions (dispatch)
const actions: ActionTree<DressingStateInterface, StateInterface> = {
  // someAction(/* context */) {
  //   // your code
  // },
  async DRESSINGS({ commit }) {
    const dressings = await repository.get_dressings();
    commit("set_dressings", dressings);
  },

  async CURRENT_DRESSING({ commit }, payload) {
    const current_dressing = await repository.get_dressing_by_id(payload);
    console.log(current_dressing);
    commit("set_current_dressing", current_dressing);
  },

  async CREATE_DRESSING({ commit }, payload) {
    const dressing = await repository.create_dressing(payload.dressing); //l'id de user ??
    commit("set_current_dressing", dressing);
  },

  async UPDATE_DRESSING({ commit }, payload) {
    const updated_dressing = await repository.update_dressing(payload);
    console.log(updated_dressing);
    commit("set_updated_dressing", updated_dressing);

  },

  async DELETE_DRESSING({ commit }, payload) {
    const deleted_dressing = await repository.delete_dressing(payload);
    console.log(deleted_dressing);
    commit("set_deleted_dressing", deleted_dressing);
  },

  async COUNT_DRESSINGS({ commit }) {
    const count = await repository.count_dressings();
    commit("set_count_dressings", count);
  },
};

export default actions;
