//getters.ts https://vuex.vuejs.org/guide/getters.html
import { GetterTree } from "vuex";
import { StateInterface } from "../index";
import { DressingStateInterface } from "./state";

//List of all getters
const getters: GetterTree<DressingStateInterface, StateInterface> = {
  // someAction(/* context */) {
  //   // your code
  // },
  all_dressings: (state) => state.all_dressings,
  user_dressings: (state) => state.user_dressings,
  current_dressing: (state) => state.current_dressing,
  selected_dressing: (state) => state.selected_dressing,
  count_dressings: (state) => state.count_dressings,
  count_user_dressings: (state) => state.count_user_dressings,
};

export default getters;
