//state.ts https://vuex.vuejs.org/guide/state.html
export interface DressingStateInterface {
  //prop: boolean;
  all_dressings: [];
  user_dressings: [];
  current_dressing: Record<string, unknown>;
  selected_dressing: [];
  count_dressings: number;
  count_user_dressings: number;
  deleted_dressing: Record<string, unknown>;
  updated_dressing: Record<string, unknown>;
}

function state(): DressingStateInterface {
  return {
    //prop: false,
    all_dressings: [],
    user_dressings: [],
    current_dressing: {},
    selected_dressing: [],
    count_dressings: 0,
    count_user_dressings: 0,
    deleted_dressing:{},
    updated_dressing:{},
  };
}

export default state;
