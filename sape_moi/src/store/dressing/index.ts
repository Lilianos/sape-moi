//index.ts Module https://vuex.vuejs.org/guide/modules.html#modules
import { Module } from "vuex";
import { StateInterface } from "../index";
import state, { DressingStateInterface } from "./state";
import actions from "./actions";
import getters from "./getters";
import mutations from "./mutations";

const dressingModule: Module<DressingStateInterface, StateInterface> = {
  namespaced: true,
  actions,
  getters,
  mutations,
  state,
};

export default dressingModule;
