//mutations.ts https://vuex.vuejs.org/guide/mutations.html
import { MutationTree } from "vuex";
import { DressingStateInterface } from "./state";

//List of all mutations (commit)
const mutation: MutationTree<DressingStateInterface> = {
  // someMutation(/* state: ExampleStateInterface */) {
  //   // your code
  // },

  //

  set_dressings(state, payload) {
    state.all_dressings = payload;
  },
  set_user_dressings(state, payload) {
    state.user_dressings = payload;
  },
  set_current_dressing(state, payload) {
    state.current_dressing = payload;
  },
  set_current_dressing_name(state, payload) {
    state.current_dressing.name = payload;
  },
  set_current_dressing_description(state, payload) {
    state.current_dressing.description = payload;
  },
  set_selected_dressing(state, payload) {
    state.selected_dressing = payload;
  },
  set_count_dressings(state, payload) {
    state.count_dressings = payload;
  },
  set_current_dressing_newImage(state, payload) {
    state.current_dressing.newImage = payload;
  },
  
  set_current_dressing_image(state, payload) {
    state.current_dressing.image = payload;
  },

  set_count_user_dressings(state, payload) {
    state.count_dressings = payload;
  },
  set_updated_dressing(state, payload) {
    state.updated_dressing = payload;
  },

  set_deleted_dressing(state, payload) {
    state.deleted_dressing = payload;
  },
};

export default mutation;
