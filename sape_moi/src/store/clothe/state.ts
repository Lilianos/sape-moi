//state.ts https://vuex.vuejs.org/guide/state.html
export interface ClotheStateInterface {
  //prop: boolean;
  all_clothes: [];
  user_clothes: [];
  current_clothe: Record<string, unknown>;
  selected_clothe: Record<string, unknown>;
  count_clothes: number;
  count_user_clothes: number;
  deleted_clothe: Record<string, unknown>;
  updated_clothe: Record<string, unknown>;
}

function state(): ClotheStateInterface {
  return {
    //prop: false,
    all_clothes: [],
    user_clothes: [],
    current_clothe: {},
    selected_clothe: {},
    count_clothes: 0,
    count_user_clothes: 0,
    deleted_clothe:{},
    updated_clothe:{},
  };
}

export default state;
