//index.ts Module https://vuex.vuejs.org/guide/modules.html#modules
import { Module } from "vuex";
import { StateInterface } from "../index";
import state, { ClotheStateInterface } from "./state";
import actions from "./actions";
import getters from "./getters";
import mutations from "./mutations";

const clotheModule: Module<ClotheStateInterface, StateInterface> = {
  namespaced: true,
  actions,
  getters,
  mutations,
  state,
};

export default clotheModule;
