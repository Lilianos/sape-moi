//getters.ts https://vuex.vuejs.org/guide/getters.html
import { GetterTree } from "vuex";
import { StateInterface } from "../index";
import { ClotheStateInterface } from "./state";

//List of all getters
const getters: GetterTree<ClotheStateInterface, StateInterface> = {
  // someAction(/* context */) {
  //   // your code
  // },
  all_clothes: (state) => state.all_clothes,
  user_clothes: (state) => state.user_clothes,
  current_clothe: (state) => state.current_clothe,
  selected_clothe: (state) => state.selected_clothe,
  count_clothes: (state) => state.count_clothes,
  count_user_clothes: (state) => state.count_user_clothes,
};

export default getters;
