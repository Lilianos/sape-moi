//Call API for clothe
import axios from "axios";

//passer dans une variable d'environement !!!!!!!!!!!!
const api_strapi = "http://benoitlouveau.duckdns.org:1337";
const api_rembg = "http://benoitlouveau.duckdns.org:5001";
const api_rembglocal = "http://10.1.1.149:5000";
export default {
  /**
   *Get all clothes
   * @returns
   */
  async get_clothes() {
    try {
      const results = await axios
        .get(api_strapi + "/clothes?_sort=name:ASC")
        .then((result) => {
          return result.data;
        });
      console.debug(results);
      return results;
    } catch (error) {
      console.debug(error);
      throw error;
    }
  },

  /**
   * Get clothe by id
   * @param id
   * @returns
   */
  async get_clothe_by_id(id: number) {
    try {
      const results = await axios.get(api_strapi + "/clothes/" + id);
      //console.log(results.data);
      return results.data;
    } catch (error) {
      console.error(error);
      throw error;
    }
  },

  /**
   * Create clothe
   * @param clothe
   */
  async create_clothe(clothe: any) {
    //l'id de user ??
    try {
      const formData = new FormData();
      if (clothe.newImage) {
        console.log(clothe, clothe.newImage);
        formData.append("files.image", clothe.newImage);

        // clothe.image = await axios
        //   .post(`${api}/upload`, formData)
        //   .then(response => {
        //     return response.data
        //   })
      }
      formData.append("data", JSON.stringify(clothe));
      const result = await axios.post(api_strapi + "/clothes/", formData);
      // const result = await axios.post(api_strapi + "/clothes/", clothe);
      console.log(result.data);
      return result.data;
    } catch (error) {
      console.error(error);
    }
  },

  /**
   * Get numbers of clothes
   * @returns
   */
  async count_clothes() {
    try {
      const result = await axios.get(api_strapi + "/clothes/count");
      return result.data;
    } catch (error) {
      console.error(error);
      throw error;
    }
  },

  /**
   * Update clothe
   * @param clothe
   * @returns
   */
  async update_clothe(clothe: any) {
    try {
      const formData = new FormData();
      if (clothe.newImage) {
        console.log(clothe, clothe.newImage);
        formData.append("files.image", clothe.newImage);

        // clothe.image = await axios
        //   .post(`${api}/upload`, formData)
        //   .then(response => {
        //     return response.data
        //   })
      }
      formData.append("data", JSON.stringify(clothe));
      const result = await axios.put(
        api_strapi + "/clothes/" + clothe.id,
        formData
      );
      // const result = await axios.put(
      //   api_strapi + "/clothes/" + clothe.id,
      //   clothe
      // );
      return result.data;
    } catch (error) {
      console.error(error);
      throw error;
    }
  },

  /**
   * Delete clothe
   * @param clothe
   */
  async delete_clothe(clothe: any) {
    try {
      const result = await axios.delete(api_strapi + "/clothes/" + clothe.id);
      return result.data;
    } catch (error) {
      console.error(error);
      throw error;
    }
  },

  /**
   * Remove background from image passed by URL
   * @param clothe
   * @returns
   */
  async image_rembg(clothe: any) {
    try {
      if (clothe.newImage) {
        const formData = new FormData();
        formData.append("file", clothe.newImage);
        const result = await axios.post(api_rembg, formData,{ responseType: "blob" });
        console.log(result.data);
        return result.data;
      } else {
        const result = await axios.get(
          api_rembg + "?url=" + api_strapi + clothe.image.url,
          { responseType: "blob" }
        );
        console.log(result.data);
        return result.data;
      }
    } catch (error) {
      console.error(error);
    }
  },
};
