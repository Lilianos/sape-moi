//actions.ts https://vuex.vuejs.org/guide/actions.html
import { ActionTree } from "vuex";
import { StateInterface } from "../index";
import { ClotheStateInterface } from "./state";
import repository from "./repository";

//List of all actions (dispatch)
const actions: ActionTree<ClotheStateInterface, StateInterface> = {
  // someAction(/* context */) {
  //   // your code
  // },

  async CLOTHES({ commit }) {
    const clothes = await repository.get_clothes();
    commit("set_clothes", clothes);
  },

  async CURRENT_CLOTHE({ commit }, payload) {
    const current_clothe = await repository.get_clothe_by_id(payload);
    console.log(current_clothe);
    commit("set_current_clothe", current_clothe);
  },

  async CREATE_CLOTHE({ commit }, payload) {
    const clothe = await repository.create_clothe(payload.clothe); //l'id de user ??
    commit("set_current_clothe", clothe);
  },

  async UPDATE_CLOTHE({ commit }, payload) {
    const updated_clothe = await repository.update_clothe(payload);
    console.log(updated_clothe);
    commit("set_updated_clothe", updated_clothe);

    // mettre à jour l'ocalement l'habit ou recharger tous les habits
  },

  async DELETE_CLOTHE({ commit }, payload) {
    const deleted_clothe = await repository.delete_clothe(payload);
    console.log(deleted_clothe);
    commit("set_deleted_clothe", deleted_clothe);

  },

  async COUNT_CLOTHES({ commit }) {
    const count = await repository.count_clothes();
    commit("set_count_clothes", count);
  },

  async REMOVE_BACKGROUND({ commit }, payload) {
    const image_rembged = await repository.image_rembg(payload)  
    commit("set_current_clothe_newImage", image_rembged)
  },
};

export default actions;
