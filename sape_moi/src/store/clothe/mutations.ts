//mutations.ts https://vuex.vuejs.org/guide/mutations.html
import { MutationTree } from "vuex";
import { ClotheStateInterface } from "./state";

//List of all mutations (commit)
const mutation: MutationTree<ClotheStateInterface> = {
  // someMutation(/* state: ExampleStateInterface */) {
  //   // your code
  // },
  set_clothes(state, payload) {
    state.all_clothes = payload;
  },

  set_user_clothes(state, payload) {
    state.user_clothes = payload;
  },

  set_current_clothe(state, payload) {
    state.current_clothe = payload;
  },

  set_updated_clothe(state, payload) {
    state.updated_clothe = payload;
  },

  set_deleted_clothe(state, payload) {
    state.deleted_clothe = payload;
  },

  set_current_clothe_name(state, payload) {
    state.current_clothe.name = payload;
  },
  set_current_clothe_description(state, payload) {
    state.current_clothe.description = payload;
  },

  set_current_clothe_newImage(state, payload) {
    state.current_clothe.newImage = payload;
  },
  set_current_clothe_image(state, payload) {
    state.current_clothe.image = payload;
  },

  set_selected_clothe(state, payload) {
    state.selected_clothe = payload;
  },

  set_count_clothes(state, payload) {
    state.count_clothes = payload;
  },

  set_count_user_clothes(state, payload) {
    state.count_clothes = payload;
  },
};

export default mutation;
