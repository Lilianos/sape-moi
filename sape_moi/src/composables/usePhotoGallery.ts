import { ref, onMounted, watch } from "vue";
import {
  Camera,
  CameraResultType,
  CameraSource,
  Photo,
} from "@capacitor/camera";
import { Filesystem, Directory } from "@capacitor/filesystem";
import { Storage } from "@capacitor/storage";

export function usePhotoGallery() {
  const takePhoto = async () => {
    let photo
    try {
       photo = await Camera.getPhoto({
        resultType: CameraResultType.Uri,
        source: CameraSource.Camera,
        quality: 100,
      });
    } catch (error) {
      console.log(error);
      
    }
    //const fileName = new Date().getTime() + ".jpeg";
    // const savedFileImage = {
    //   filepath: fileName,
    //   photo: photo.webPath,
    // };
    //const savedFileImage = await savePicture(photo, fileName);

    //photos.value = [savedFileImage, ...photos.value];
    myphoto.value = photo
  }; 

  const pickPhoto = async () => {
    let photo
    try {
       photo = await Camera.getPhoto({
        resultType: CameraResultType.Uri,
        source: CameraSource.Photos,
        quality: 100,
      });
    } catch (error) {
      console.log(error);
      
    }
    //const fileName = new Date().getTime() + ".jpeg";
    // const savedFileImage = {
    //   filepath: fileName,
    //   photo: photo.webPath,
    // };
    //const savedFileImage = await savePicture(photo, fileName);

    //photos.value = [savedFileImage, ...photos.value];
    myphoto.value = photo
  }; 

  
  const myphoto = ref<Photo>()
  const photos = ref<UserPhoto[]>([]);



  const convertBlobToBase64 = (blob: Blob) =>
  new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.onerror = reject;
    reader.onload = () => {
      resolve(reader.result);
    };
    reader.readAsDataURL(blob);
  });

  const savePicture = async (photo: Photo, fileName: string): Promise<UserPhoto> => {
    let base64Data = "";
  
    // Fetch the photo, read as a blob, then convert to base64 format
    const response = await fetch(photo.webPath!);
    const blob = await response.blob();
    base64Data = (await convertBlobToBase64(blob)) as string;
  
    const savedFile = await Filesystem.writeFile({
      path: fileName,
      data: base64Data,
      directory: Directory.Data,
    });
  
    // Use webPath to display the new image instead of base64 since it's
    // already loaded into memory
    return {
      filepath: fileName,
      webviewPath: photo.webPath,
    };
  };


  return {
    takePhoto,
    pickPhoto,
    photos,
    myphoto,
  };
}

export interface UserPhoto {
  filepath: string;
  webviewPath?: string;
}
