# Sape moi

## Préambule
Ce projet innovant est réalisé par un groupe d’étudiants, commun au Campus Numérique de Nevers, venant de l’école CS2I et de l’école DIGISUP. Ils composent une équipe qui se complète tant en termes de rôles qu’en termes de compétences.

Il est planifié sur l’année complète d’octobre 2021 à avril 2022 et a pour but de répondre à un besoin actuel d’un groupe d’individus et de pouvoir être présenté comme une solution innovante.

## Le contexte

Un mardi matin d’octobre avec notre intervenant M.Blondet, s’est déroulée une première rencontre entre les étudiants Digisup et Cs2i. Présentations générales, Ice breaker et café dans une ambiance conviviale avant de pouvoir débuter les présentations d’idées de projet innovant pour cette première année de Mastère. Les Cs2i ont découvert à ce moment-là qu'il fallait préparer des idées pour ce jour (un problème d’envoi de mail pour la liste de diffusion cs2i). C’est alors qu’un membre de l’équipe, Lilian a pensé à une idée de projet 3 minutes avant de lever la main pour présenter son idée. Un outil pour aider les personnes pressées le matin, éternels indécis et acheteurs compulsifs qui déclarent toujours ne rien avoir à se mettre. 

Peu après les présentations des idées terminées, les noms des projets ont été écrit sur une feuille, et M.Blondet a distribué 5 gommettes de couleur à chacun et nous avons pu “voter” pour les projet les plus intéressant et innovants.
Ensuite, les projets retenus ont pu constituer des équipes de travail avec un équilibre en termes de proportion Cs2i/Digisup c’est alors que le projet Sape-moi est né.

Enfin, l’après-midi avait comme objectif de pouvoir présenter notre projet devant un Jury constitué ce jour-là de M. Desramé, M.Robbe et de M. Moreau. Nous avons donc travaillé en équipe pour mettre en commun nos différentes idées et avons donné au projet une direction commune dans laquelle nous allons nous diriger. 
